# CHEMISE

## What is CHEMISE?

CHEMISE (CHEMIcal System Evaluator) is a Python library to define and
evaluate systems of chemical reactions.  Some of its features are

* **Pythonic.**
  Everywhere, CHEMISE strives to be as pythonic as possible.  Only Python
  is required to use it, altough the permormance-sensitive parts of the CHEMISE
  kernel are written in Fortran.

* **Vectorized.**
  CHEMISE is designed to work on spatial arrays.  This means that chemical
  computations can be applied to a full array in a single instruction.

* **Modular.**
  CHEMISE separates chemical computations from the rest of your code.
  Thus, a CHEMISE module can be use as a drop-in to use complex chemistry
  in many other codes, such as diffusion or advection codes.

* **Self-documenting.**
  CHEMISE supports LaTeX output of your chemical module for self-documentation.


## Installation

To install CHEMISE, clone this repository and run, in the `src/` folder::
```
    python setup.py install
```

## First steps

Let us now go through an example of using chemise, included in the file
`samples/argon.py`.  Using a simplified chemistry for an Argon plasma we describe
some of CHEMISE's features and how to use CHEMISE to integrate the evolution

First one has to import the chemise module:

```python
  import chemise as ch
```

### Defining a reaction set

Now we are ready to describe the chemistry module.  In CHEMISE chemistry modules
are instances of a class derived from the `ReactionSet` class:

```python
    class Argon(ch.ReactionSet):
          def __init__(self):
              super(Argon, self).__init__()
```

Here we have just defined the class and invoked the `__init__` method of
`ReactionSet`, which will perform some initialization stuff.

The next step is to tell CHEMISE which species in the chemical system should
be considered as having a fixed density.  These are usually abundant, neutral
species with such large density that chemical processes barely their
concentration.  We set the fixed species with the ``fix`` method:

```python
          self.fix({'Ar': GAS_DENSITY})
```
As you can see, `fix` receives a dictionary that associates species names and
their fixed density.  In our case we are setting the argon density to
10<sup>20</sup> m<sup>-3</sup>.

Now let us specify some reactions.  Let us start with the impact excitation of
ground-state Ar into a metastable Ar<sup>\*</sup>.  The rate coefficient
for this reaction depends on the reduced electric field *E/n*.  In general
a rate coefficient may depend on a number of parameters; besides the reduced
electric field, rates usually depend on the gas temperature.  CHEMISE handles
this dependency by generally defining rates as functions of these parameters.
When you define a chemical reaction set you decide which parameters you will
use and then define all rates as functions of these paramteres, always in the
same order.  It's ok if sometimes these functions ignore some of the parameters.

In our case we will take the field-dependent reaction rate from an external
file `swarm/rate_00.dat`.  This file contains two two colums: reduced electric
field and the corresponding rate coefficient.  We will interpolate between the
listed values and, since we deal with variations of many orders of magnitude
between the listed, we opt for using a logarithmic interpolation:

```python
        self.add("e + Ar -> e + Ar*",
                 ch.LogLogInterpolate0("swarm/rate_00.dat"))

        self.add("e + Ar -> e + e + Ar+",
                 ch.LogLogInterpolate0("swarm/rate_01.dat"))

```
Here `add` is the method that adds a reaction.  As first argument it takes a
string with the reaction signature and as second argument a function (or
a *callable*) to calculate a rate.  Here `LogLogInterpolate0` reads from
a file and constructs a callable that interpolates from the file's values.
The `0` at the end of the name means that the resulting callable will take
an arbitrary number of parameters but only use the first one (zero in Python's
convention) to interpolate.

As we mentioned above, the second argument of the `add` method takes any
callable.  However, we often want our rate coefficients to be a constant instead
of a function.  We can wrap these rates in e.g. a lambda function; however
CHEMISE provides a shortcut with a `Constant` method.  We use it to specify
the rate of recombination:

```python
        self.add("Ar+ + e + e -> Ar + e",
                 ch.Constant(1e-38))
```

We now finish the initialization of the `ReactionSet` with a call to the
`initialize` method, which takes care of building all the internal data
required for further computations:

```python
        self.initialize()
```

### Calculations with reaction sets.

A class derived from `ReactionSet` defines a chemistry.  Now let's see what
we can do with these definitions.  We here define a `main` method and instantiate
the class `Argon`:

```python
def main():
    rs = Argon()
```

We can print a short summary of the species and reactions contained in the
reaction set using

```python
    rs.print_summary()
```

The `ReactionSet` contains only information about reactions, species and their
relationship but does not provide space to store densities in a particular
realization of the chemistry.  However, it contains methods to deal with these
densities.  For example we may allocate space for the densities of all species
using

```python
    n0 = rs.zero_densities(1)
```

where the argument (`1`) is the number of cells where we want densities to be
defined.  As its name implies `zero_densities` initializes all densities to zero.
We can now give values to the densities of certain species:

```python
    rs.set_species(n0, 'e', 1 * co.centi**-3)
    rs.set_species(n0, 'Ar+', 1 * co.centi**-3)
```

We will now integrate the evolution of the chemical system for a given value
of reduced electric field, in this case 15 Td.  We integrate the system up
to one millisecond with output at 1000 instants.  For this we define

```python
    en = np.array([15.0])
    time = np.linspace(0, 1e-3, 1000)
```

Here we define the reduced field as a 1-element array because CHEMISE expects
all parameters to be arrays with size equal to the number of cells.

Now, in order to use one of scipy's ODE solvers, we define a function that
returns the time derivatives of the densities as well as the jacobian.
CHEMISE provides shortcuts fot this:

```python
    def f(t, n):
        return rs.fderivs(n[:, np.newaxis], en)

    def jac(t, n):
        return rs.fjacobian(n, en)
```

Now we are ready to integrate the ODE system using scipy´s own solver:

```python
    r = ode(f)
    r.set_integrator('vode', method='bdf', nsteps=2000,
                     atol=np.full_like(n0, 1e-3),
                     rtol=np.full_like(n0, 1e-8))

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = np.squeeze(n0)


    for i, it in enumerate(time[1:]):
        n[:, i + 1] = np.squeeze(r.integrate(it))
```

And, finally, plot the results using matplotlib:


```python
    for s in rs.species:
        plt.plot(time, rs.get_species(n, s), label=s)

    plt.semilogy()
    plt.legend()
    plt.show()
```


